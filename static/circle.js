function startGame() {
    if (state.started) {
        return;
    }
    state.started = true;


    document.getElementById("draw_area").style.visibility = "visible";

    var canvas =  document.getElementById('canvas');
    var context = canvas.getContext("2d");

    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var proof = new Array();
    var paint;
    var can_paint = false;

    canvas.onmousedown = function(e){
        if (!can_paint) return;

        var rect = canvas.getBoundingClientRect();

        var mouseX = e.clientX - rect.left;
        var mouseY = e.clientY - rect.top;

        paint = true;
        addClick(mouseX, mouseY);
        redraw();
    };

    canvas.onmousemove = function(e){
        if(paint && can_paint){
            var rect = canvas.getBoundingClientRect();
            addClick(e.clientX - rect.left, e.clientY - rect.top, true);
            redraw();
        }
    };

    canvas.onmouseup = function(e){
        paint = false;
        can_paint = false;
    };

    canvas.onmouseleave =function(e){
        paint = false;
    };


    function addClick(x, y, dragging)
    {
        clickX.push(x);
        clickY.push(y);
        proof.push((Math.floor(x)+Math.floor(y)*1000) ^ state.seed);
        clickDrag.push(dragging);
    }

    function redraw(){

        context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas

        context.strokeStyle = "#66FFFF";
        context.lineJoin = "round";
        context.lineWidth = 3;

        for(var i=0; i < clickX.length; i++) {

            context.beginPath();
            if(clickDrag[i] && i){
                context.moveTo(clickX[i-1], clickY[i-1]);
            }else{
                context.moveTo(clickX[i]-1, clickY[i]);
            }
            context.lineTo(clickX[i], clickY[i]);
            context.closePath();
            context.stroke();
        }
    }

    var countdown_text = document.getElementById("countdown_text");
    var countdown = document.getElementById("countdown");

    var prepare_time = 5000;
    var game_time = 5000;
    var tick = 500;
    var prepare = true;

    countdown.textContent = toS(prepare_time);

    function toS(i) {
        return Math.ceil(i / 1000).toString();
    }

    function timedown() {

        if (prepare) {
            prepare_time -= tick;

            countdown.textContent= toS(prepare_time);

            if (prepare_time <= 0) {
                prepare = false;
                countdown_text.textContent = "Draw!";
                countdown.textContent = toS(game_time);
                can_paint = true;
                canvas.classList.add("can-draw");
            }

            setTimeout(timedown, tick)
        } else {
            game_time -= tick;

            countdown.textContent = toS(game_time);

            if (game_time <= 0) {
                countdown_text.textContent = "Judging you!";
                countdown.textContent ="";
                can_paint = false;
                canvas.classList.remove("can-draw");
                redraw();

                var data  = {
                    x : clickX,
                    y : clickY,
                    d : clickDrag,
                    p : proof
                };

                console.log(proof);

                sendMessage("/results", btoa(JSON.stringify(data)));
            } else {
                setTimeout(timedown, tick)
            }
        }
    }

    setTimeout(timedown, tick)
}


function drawResults(players) {

    for (var i = 0; i < players.length; i++) {
        document.querySelector("#user"+i+" .position").textContent = "Place: " + players[i].place.toString();
        document.querySelector("#user"+i+" .score").textContent = "Score: " + players[i].score.toFixed(2).toString();
    }

    var countdown_text = document.getElementById("countdown_text");

    function placeToText(place) {
        return {
            1: "You made the best circle!",
            2: "Not bad, you finished second",
            3: "Not roundy enough, last place"
        }[place];
    }

    countdown_text.textContent = placeToText(players[state.my_place].place);
    countdown.textContent = "Score: " + players[state.my_place].score.toFixed(2).toString();

    document.getElementById("again").style.visibility = "visible";
}

function onOpened() {
    sendMessage('/get_game')
}

function onMessage(m) {
    data = JSON.parse(m.data);

    for (var i = 0; i < data.players.length; i++) {
        var player = data.players[i];

        document.querySelector("#user"+i+" .name").textContent = player.nickname;
        if (player.nickname == state.me) {
            document.querySelector("#user"+i).classList.add("its-me");
            state.my_place = i;
        }
    }
    if (data.ready && !state.started) {
        state.seed = data.seed;
        startGame();
        state.started = true;
    }

    if (data.results) {
        drawResults(data.players)
    }
}

sendMessage = function (path, circle) {
    path += '?g=' + state.game_key;

    var xhr = new XMLHttpRequest();
    xhr.open('POST', path, true);
    if (circle) {
        var p = "circle="+circle;
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(p);
    } else {
        xhr.send();
    }
};

openChannel = function () {
    var token = state.token;
    var channel = new goog.appengine.Channel(token);
    var handler = {
        'onopen': onOpened,
        'onmessage': onMessage,
        'onerror': function () {
        },
        'onclose': function () {
        }
    };
    var socket = channel.open(handler);
    socket.onopen = onOpened;
    socket.onmessage = onMessage;
};
