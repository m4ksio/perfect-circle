import webapp2
import math
import jinja2
import os
import json
import random
import base64
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.api import channel
import logging

MAX_PLAYERS = 3
BOARD_SIZE = 400
PERFECT_RADIUS = 100
CENTER = BOARD_SIZE / 2
JUDGE_SEGMENTS = 32

class Player(ndb.Model):
    user = ndb.UserProperty()
    circle = ndb.TextProperty()
    place = ndb.IntegerProperty()
    score = ndb.FloatProperty()

class Room(ndb.Model):
    players = ndb.StructuredProperty(Player, repeated=True)
    players_count = ndb.IntegerProperty(default=1)
    seed = ndb.IntegerProperty()

class MainPage(webapp2.RequestHandler):
    def get(self):
        logging.info('Starting Main handler')
        # Checks for active Google account session
        user = users.get_current_user()

        if not user:
            self.redirect(users.create_login_url(self.request.uri))
            return

        rooms = Room.query(Room.players_count < MAX_PLAYERS).order(Room.players_count).fetch(1)

        room = rooms[0] if rooms else None

        if room is None:
            room = Room(players=[Player(user = user),], seed=random.randint(128, 1024))
        else:

            room.players.append(Player(user=user))
            room.players_count += 1

        room.put()

        token = channel.create_channel(user.user_id() + str(room.key.id()))

        template_values = {
            'my_nickname': user.nickname(),
            'room': room,
            'token': token,
            'game_key': room.key.id(),
            'board_size': BOARD_SIZE,
            'perfect_radius': PERFECT_RADIUS,
            'center': CENTER,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))


def send(room):

    def dump_player(player):
        return {
            'nickname': player.user.nickname(),
            'place': player.place,
            'score': player.score
        }

    ready = room.players_count == MAX_PLAYERS

    msg = json.dumps({
        'players' : [dump_player(player) for player in room.players],
        'results' : all(player.place for player in room.players),
        'ready': ready,
        'seed': room.seed if ready else 0,

        })

    for player in room.players:
        channel.send_message(player.user.user_id() + str(room.key.id()), msg)

class GetGame(webapp2.RequestHandler):

    def post(self):
        game_key = self.request.get('g')
        room = Room.get_by_id(long(game_key))
        send(room)


class Judge(webapp2.RequestHandler):

    def decode(self, blob):
        debased = base64.b64decode(blob)
        data = json.loads(debased)

        return data

    def judge(self, data, room):

        def distance(x1, y1, x2, y2):
            return math.sqrt( (x1-x2)**2 + (y1-y2)**2)

        def intersect(x1, y1, x2, y2, x3, y3, x4, y4):

            x12 = x1 - x2
            x34 = x3 - x4
            y12 = y1 - y2
            y34 = y3 - y4

            c = x12 * y34 - y12 * x34

            if math.fabs(c) < 0.01:
                # No intersection
                return False
            else:
                # Intersection
                a = x1 * y2 - y1 * x2
                b = x3 * y4 - y3 * x4

                x = (a * x34 - b * x12) / c
                y = (a * y34 - b * y12) / c

                u = math.fabs(distance(x, y, x3, y3) + distance(x, y, x4, y4) - distance(x3, y3, x4, y4)) < 0.1
                v = math.fabs(distance(x, y, x1, y1) + distance(x, y, x2, y2) - distance(x1, y1, x2, y2)) < 0.1

                if u and v:
                    return {'x':x, 'y':y}
                else:
                    return False

        x1 = CENTER
        y1 = CENTER

        r = BOARD_SIZE /2.0 * math.sqrt(2.0)

        segments = JUDGE_SEGMENTS

        step = math.pi*2.0 / segments

        angle = 0

        points =[]

        ideal_r = PERFECT_RADIUS

        sum = 0

        circleX = data['x']
        circleY = data['y']
        drag = data['d']
        proof = data['p']

        for j in range(len(circleX)):
            check = (int(circleX[j])+int(circleY[j])*1000) ^ room.seed
            if proof[j] != check:
                #chaeter detected
                return segments * ideal_r * 10

        for k in range(segments):
            angle = step * k

            x2 = x1 + r * math.sin(angle)
            y2 = y1 + r * math.cos(angle)

            segment_matches = []

            for j in range(len(circleX)):
                if drag[j] and j>0:
                    x3 = circleX[j - 1]
                    y3 = circleY[j - 1]
                else:
                    x3 = circleX[j] - 1
                    y3 = circleY[j]

                x4 = circleX[j]
                y4 = circleY[j]

                result = intersect(x1, y1, x2, y2, x3, y3, x4, y4)

                if result is not False:
                    segment_matches.append(result)

            dist = r
            the_match = None

            for match in segment_matches:

                c = distance(x1, y1, match['x'], match['y'])

                if c < dist:
                    dist = c
                    the_match = match

            if the_match:

                diff = math.fabs(ideal_r - distance(x1, y1, the_match['x'], the_match['y']))

                sum += diff
            else:
                sum += ideal_r

        return sum

    @ndb.transactional
    def post(self):
        game_key = self.request.get('g')
        user = users.get_current_user()
        room = Room.get_by_id(long(game_key))
        circle_blob = self.request.get('circle')

        circle = self.decode(circle_blob)

        score = self.judge(circle, room)

        # player = next(player for player in room.players if player.user == user)
        players = filter(lambda p: p.user == user, room.players)

        # player.score = score
        for player in players:
            player.score = score

        room.put()

        if all(player.score for player in room.players):

            by_score = sorted(enumerate([player.score for player in room.players]), key = lambda x: x[1])

            place = 1

            for b in by_score:
                room.players[b[0]].place = place
                place += 1

            room.put()

            send(room)


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/get_game', GetGame),
    ('/results', Judge)
    ], debug=False)

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)